const jwt = require('jsonwebtoken');
const config = require('../config/config.js');

exports.requireLogin = function(req, res, next) {
  try {
    const token = jwt.verify(req.headers.authorization, config.JWT_SECRET);
    req.userId = token.id;
    next();
  } catch  (error) {
    return res.status(401).send({message: 'Authentication failed.'});
  }

}
