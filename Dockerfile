# Imagen docker base
FROM node

# Definimos directorio de trabajo docker
WORKDIR api-image

# directorio donde copiamos la imagne
ADD . /api-image

# RUN npm install

# Puerto donde exponemos la aplicación
EXPOSE 3000

# Comando para lanzar la app
CMD ["npm", "start"]
