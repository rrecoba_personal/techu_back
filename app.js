// imports config for this project
const config = require('./config/config.js');

// imports required modules and initialize express
const express = require("express");
const bodyParser = require('body-parser');
const requestJson = require('request-json');
const logger = require('morgan');
const app = express();
const cors = require('cors')

// Init Server
app.listen(config.SERVER_PORT);
console.log('Escuchando en el puerto ' + config.SERVER_PORT + ' ... ');

/*** Setup Middleware ***/

// Log requests to API using morgan
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(cors());

// Routes
const router = require('./router');
router(app);
