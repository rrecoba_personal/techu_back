/* Account Class */
class Account {

  constructor (id,currency, name, balance) {
    this.id = id;
    this.name =  name;
    this.balance = balance;
    this.currency = currency;
    this.transactions = [];
  }

  setProperties(obj){
    this.name =  obj.name;
    this.balance = obj.balance;
    this.currency = obj.currency;
  }

  /** Getter */
  getName(){
    return this.name;
  }

  /** Setter */
  setName(name){
    this.name = name;
  }

  /** Getter */
  getBalance(){
    return this.balance;
  }

  /** Setter */
  setBalance(balance){
    this.balance = balance;
  }

  /** Getter */
  getCurrency(){
    return this.currency;
  }

  /** Setter */
  setCurrency(currency){
    this.currency = currency;
  }

}

module.exports = Account;
