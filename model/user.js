const Account = require('./account.js');

/* User Class */
class User {

  constructor(id, firstName,lastName,email, password){
    this.id = id;
    this.firstName =  firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password
    this.accounts = [];
  }

  initAccounts() {
    this.accounts = [];

    // Create UYU account
    let idUYU = parseInt(Math.random() * 10000000);
    let ctaUYU = new Account(idUYU, 'UYU', 'Caja de Ahorro UYU', 0);
    this.accounts.push(ctaUYU);

    // Create USD account
    let idUSD = parseInt(Math.random() * 10000000);
    let ctaUSD = new Account(idUSD, 'USD', 'Caja de Ahorro USD', 0);
    this.accounts.push(ctaUSD);
  }

  /** Getter */
  getId(){
    return this.id;
  }

  /** Setter */
  setId(id){
    this.id = id;
  }

  /** Getter */
  getFirstName(){
    return this.firstName;
  }

  /** Setter */
  setFirstName(firstName){
    this.firstName = firstName;
  }

  /** Getter */
  getLastName(){
    return this.lastName;
  }

  /** Setter */
  setLastName(lastName){
    this.lastName = lastName;
  }

  /** Getter */
  getEmail(){
    return this.email;
  }

  /** Setter */
  setEmail(email){
    this.email = email;
  }

  /** Getter */
  getPassword(){
    return this.password;
  }

  /** Setter */
  setPassword(password){
    this.password  = password;
  }

}

module.exports = User;
