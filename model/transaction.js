const utils = require('../helpers/utils.js')

/* Account Class */
class Transaction {

  constructor (userId, accountId, description, amount, type) {
    this.user_id = userId;
    this.account_id =  accountId;
    this.description = description;
    this.amount = amount;
    this.type = type;
    this.date =utils.getDate();
  }

  /*setProperties(obj){
    this.name =  obj.name;
    this.balance = obj.balance;
    this.currency = obj.currency;
  }*/

  /** Getter */
  geUserId(){
    return this.user_id;
  }

  /** Setter */
  setUserId(userId){
    this.user_id = userId;
  }

  getAccountId(){
    return this.account_id;
  }

  setAccountId(accountId){
    this.account_id = accountId;
  }

  getDescription(){
    return description;
  }

  setDescription(description){
    this.description = description
  }

  getAmount(){
    return amount;
  }

  setAmount(amount){
    this.amount = amount;
  }

  getType(){
    return this.type;
  }

  setType(type) {
    this.type = type;
  }
}


module.exports = Transaction;
