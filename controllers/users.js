// imports config for this project
const config = require('../config/config.js');

// imports required modules
const requestJson = require('request-json');
const bcrypt = require('bcryptjs');
const inputValidator = require('./validator.js');
const User = require('../model/user.js');


function nextUserId(callback) {
  let maxUserId = 1;
  let clienteMlab = requestJson.createClient(config.URL_MLAB);
  let queryObject = { "id": -1 };
  let queryStringEmail = 's=' + JSON.stringify(queryObject) + '&l=1&' + config.API_KEY;

  // get user with max ID
  clienteMlab.get('user?' + queryStringEmail, function(error, respuestaM , body) {
    console.log(body);
    if (!error) {
      if (body.length > 0) {
        maxUserId = body[0].id + 1;
      }
    } else {
      maxUserId = -1;
    }
    callback(maxUserId);
  });
}

function getUserByMail(email, callback) {
  let queryObject = { "email": email  };
  let filterObject = {"accounts.transactions": 0};
  let queryStringEmail = 'q=' + JSON.stringify(queryObject) + '&' + 'f=' + JSON.stringify(filterObject) + '&'+ config.API_KEY;

  let clienteMlab = requestJson.createClient(config.URL_MLAB);
  console.log(queryStringEmail);
  clienteMlab.get("user?" + queryStringEmail , function(err, resM, body) {
    var response = {};
    if(err) {
        throw new Error("Error consultando usuario por mail en mlab!");
    } else {
      if(body.length > 0) {
        response = body;
      }
    }
    console.log(response);
    callback(response);
  });
}

function getUserByMail(email, callback) {
  let queryObject = { "email": email  };
  let filterObject = {"accounts.transactions": 0};
  let queryStringEmail = 'q=' + JSON.stringify(queryObject) + '&' + 'f=' + JSON.stringify(filterObject) + '&'+ config.API_KEY;

  let clienteMlab = requestJson.createClient(config.URL_MLAB);
  console.log(queryStringEmail);
  clienteMlab.get("user?" + queryStringEmail , function(err, resM, body) {
    var response = {};
    if(err) {
        throw new Error("Error consultando usuario por mail en mlab!");
    } else {
      if(body.length > 0) {
        response = body;
      }
    }
    console.log(response);
    callback(response);
  });
}

/**
 * @api {post} /users/  Register
 * @apiName Register
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {String} email User's email
 * @apiParam {String} firstName User's firstName
 * @apiParam {String} lastName User's firstName
 * @apiParam {String} password User password
 *
 * @apiSuccess {String}  msg Message
 * @apiSuccess {User}  response User Detail
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {
 *    "msg": "Usuario registrado exitosamente",
 *    "response": {
 *        "id": 4,
 *        "firstName": "rodrigo",
 *         "email": "r1@r.com",
 *         "password": "$2a$10$sWhRqqvQpp1WhRV3v3./LeIy5Lj1cM3j0lFsbtVtxxyc7ina0wnjG",
 *         "accounts": [
 *             {
 *                 "id": 7285434,
 *                 "name": "Caja de Ahorro UYU",
 *                 "balance": 0,
 *                 "currency": "UYU",
 *                 "transactions": []
 *             },
 *             {
 *                 "id": 1582414,
 *                 "name": "Caja de Ahorro USD",
 *                 "balance": 0,
 *                 "currency": "USD",
 *                 "transactions": []
 *             }
 *         ],
 *         "_id": {
 *             "$oid": "5c0da5641f6e4f682f4b3c81"
 *         }
 *     }
 * }
 *
 * @apiError 400 Bad Request
 *
 * @apiErrorExample Error-404:
 *     HTTP/1.1 400 Bad Reuqest
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 409 Conflict
 *
 * @apiErrorExample Error-409:
 *     HTTP/1.1 409 Conflict
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 500 Internal Server Error
 *
 * @apiErrorExample Error-500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "msg": "Error message"
 *     }
 */
exports.register = function(req, res, next) {

  let newUser = new User(0, req.body.firstName, req.body.lastName, req.body.email, req.body.password);
  let result = inputValidator.validateUserInputs(newUser);

  if (result.valid) {

    // Verifico que no exista un usuario con ese mail
    getUserByMail(newUser.email, function (user) {

      if (user.length > 0) {
        res.status(409).send({"msg": "El email ingersado ya se encuentra registrado"});
      } else {

        // Obtengo el id para ese usuario
        nextUserId(function registerUser(nextId) {
          newUser.setId(nextId);
          newUser.setPassword(bcrypt.hashSync(req.body.password, 10));
          newUser.initAccounts();

          let clienteMlab = requestJson.createClient(config.URL_MLAB);
          clienteMlab.post("user?" + config.API_KEY, newUser,
            function(error, respuestaMLab, body){
              if (!error) {
                let respone = {};
                if (error) {
                  response = {
                    "msg" : "Ha ocurrido un error al registar el usuario."
                  }
                } else {
                  response = body;
                }
                res.send({msg: "Usuario registrado exitosamente", response});
              } else {
                res.status(500).send({msg: "Ha ocurrido un error"});
              }
          });
        });
      }
    });
  } else {
    res.status(400).send(result.errors);
  }
}

// GET Users by eMail
exports.getUserByMail = function(email, callback) {
  getUserByMail(email, callback);
}





/**
 * @api {get} /users/  Get Users
 * @apiName GetUsers
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiHeader {String} authorization Users JWT token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhMzE0YmFjNzY1Nzc2MGM1YWRkNTM2MiIsImZpcnN0TmFtZSI6IkFyZWwiLCJsYXN0TmFtZSI6IlNpcmluIiwiZW1haWwiOiJhcmVsc2lyaW4rMUBnbWFpbC5jb20iLCJpYXQiOjE1MTY3MzQyNDgsImV4cCI6MTUxNjc0NDMyOH0.mQzSgw_A5PdFfyRs88BZgyBUvmZhzrrmfw2SZWp6kUw"
 *     }
 *
 * @apiSuccess {User}  response User Detail
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *        {
 *            "id": 1,
 *            "firstName": "Rodrigo",
 *            "lastName": "Recoba",
 *            "email": "r@r.com",
 *            "password": "$2a$10$Nj2i7LJx1kssqY/MlboRre5ilVdHx/cHIaHtJ1NRQaYXZsvhnb28u",
 *            "accounts": [
 *                {
 *                    "id": 5433459,
 *                    "name": "Caja de Ahorro UYU",
 *                    "balance": 0,
 *                    "currency": "UYU",
 *                    "transactions": []
 *                },
 *                {
 *                    "id": 5340606,
 *                    "name": "Caja de Ahorro USD",
 *                    "balance": 0,
 *                    "currency": "USD",
 *                    "transactions": []
 *                }
 *            ]
 *        },
 *        {
 *            "id": 2,
 *            "firstName": "Rodrigo Viana",
 *            "lastName": "Recoba",
 *            "email": "rodri@gmail.com",
 *            "password": "$2a$10$Sx7GMhoKPQEClI4Zxrz2h.vLAUpfUlVztqhlJm8XJjDLUOA47NDKa",
 *            "accounts": [
 *                {
 *                    "id": 326925,
 *                    "name": "Caja de Ahorro UYU",
 *                    "balance": -550.24,
 *                    "currency": "UYU",
 *                    "transactions": []
 *                },
 *                {
 *                    "id": 2192907,
 *                    "name": "Caja de Ahorro USD",
 *                    "balance": 847,
 *                    "currency": "USD",
 *                    "transactions": []
 *                }
 *            ]
 *        }
 *  ]
 *
 * @apiError 401 Unauthorized
 *
 * @apiErrorExample Error-401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 404 Not Found
 *
 * @apiErrorExample Error-404:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 500 Internal Server Error
 *
 * @apiErrorExample Error-500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "msg": "Error message"
 *     }
 */
exports.getUsers = function(req, res, next) {
  let queryString = "f={'_id':0}&" + config.API_KEY;
  let httpClient = requestJson.createClient(config.URL_MLAB);

  httpClient.get('user?' + queryString, function(err, resM, body) {
    var response = {};

    if (!err) {
      if (body.length > 0) {
         response = body;
         res.status(200);
       } else {
         response = {
           "msg" : "No se han encontrado usuarios."
         }
       res.status(404);
       }
     } else {
        response = {
          "msg" : "Error obteniendo los usuarios del sistema."
        }
        res.status(500);
     };
     res.send(response);
   });
}

// GET user by id
exports.getUserById = function (req, res, next) {
  let id = req.params.id;
  let query = 'q={"id":' + id + '}&';
  let queryFilter = query + 'f={"_id":0}&' + config.API_KEY;
  let httpClient = requestJson.createClient(config.URL_MLAB);

  httpClient.get("user?" + queryFilter , function(err, resM, body) {
    var response = {};
    if(err) {
        response = {
          "msg" : "Error al obtener el usuario."
        }
        res.status(500);
    } else {
      if(body.length > 0) {
        response = body;
        res.status(200);
      } else {
        response = {
          "msg" : "No se encontro el Usuario."
        }
        res.status(404);
      }
    }
    res.send(response);
  });
};

// PUT 'users'
exports.realUpdateUser = function(req, res, next) {
  let idUpdate = req.params.id;
  let queryString = 'q={"id": '+ idUpdate + '}&' + config.API_KEY;

  let newUser = new User();
  newUser.setId(req.body.id);
  newUser.setFirstName(req.body.first_name);
  newUser.setLastName(req.body.last_name);
  newUser.setEmail(req.body.email);

  let clienteMlab = requestJson.createClient(config.URL_MLAB);
  clienteMlab.put('user?' + queryString, newUser,
      function(error, respuestaMLab, body){
        let response = {};
        if(error) {
            response = {
              "msg" : "Error actualizando la información del usuario."
            }
            res.status(500);
        } else {

          if (body.n > 0) {
            response = {
              "msg" : "Usuario actualizado exitosamente."
            }
            res.status(200);
          } else {
            response = {
              "msg" : "No se encontró el Usuario."
            }
            res.status(404);
          }
          res.send(response);
        }
      });
};


/**
 * @api {put} /users/:id  Modity User
 * @apiName ModifyUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiHeader {String} authorization Users JWT token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhMzE0YmFjNzY1Nzc2MGM1YWRkNTM2MiIsImZpcnN0TmFtZSI6IkFyZWwiLCJsYXN0TmFtZSI6IlNpcmluIiwiZW1haWwiOiJhcmVsc2lyaW4rMUBnbWFpbC5jb20iLCJpYXQiOjE1MTY3MzQyNDgsImV4cCI6MTUxNjc0NDMyOH0.mQzSgw_A5PdFfyRs88BZgyBUvmZhzrrmfw2SZWp6kUw"
 *     }
 *
 * @apiParam {String} firstName User's firstName
 * @apiParam {String} lastName User's firstName
 * @apiParam {String} password User password
 *
 * @apiSuccess {String}  msg Messge
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *      "msg": "Usuario actualizado exitosamente."
 *     }
 *
 * @apiError 400 Bad Request
 *
 * @apiErrorExample Error-400:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "msg": "Error message"
 *     }
 *
 * @apiError 401 Unauthorized
 *
 * @apiErrorExample Error-401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 404 Not Found
 *
 * @apiErrorExample Error-404:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 500 Internal Server Error
 *
 * @apiErrorExample Error-500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "msg": "Error message"
 *     }
 */
exports.updateUser = function(req, res, next) {
  let idUpdate = req.params.id;
  let firstName = req.body.firstName;
  let lastName = req.body.lastName;
  let queryString = 'q={"id": '+ idUpdate + '}&' + config.API_KEY;

  let result = inputValidator.validateUpdateInputs(firstName, lastName, req.body.password);

  if (result.valid) {
    let password = bcrypt.hashSync(req.body.password, 10)
    let updUser = { "$set" : { "firstName" : firstName,
                               "lastName" : lastName,
                               "password" : password } };

    let clienteMlab = requestJson.createClient(config.URL_MLAB);
    clienteMlab.put('user?' + queryString, updUser,
        function(error, respuestaMLab, body){
          let response = {};
          if(error) {
              response = {
                "msg" : "Error actualizando la información del usuario."
              }
              res.status(500);
          } else {

            if (body.n > 0) {
              response = {
                "msg" : "Usuario actualizado exitosamente."
              }
              res.status(200);
            } else {
              response = {
                "msg" : "No se encontró el Usuario."
              }
              res.status(404);
            }
            res.send(response);
          }
      });
    } else {
        res.status(400).send(result.errors)
    }
};


/**
 * @api {del} /users/:id  Delete User
 * @apiName DeleteUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiHeader {String} authorization Users JWT token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhMzE0YmFjNzY1Nzc2MGM1YWRkNTM2MiIsImZpcnN0TmFtZSI6IkFyZWwiLCJsYXN0TmFtZSI6IlNpcmluIiwiZW1haWwiOiJhcmVsc2lyaW4rMUBnbWFpbC5jb20iLCJpYXQiOjE1MTY3MzQyNDgsImV4cCI6MTUxNjc0NDMyOH0.mQzSgw_A5PdFfyRs88BZgyBUvmZhzrrmfw2SZWp6kUw"
 *     }
 *
 *
 * @apiSuccess {String}  msg Messge
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *      "msg": "Usuario eliminado exitosamente."
 *     }
 *
 * @apiError 401 Unauthorized
 *
 * @apiErrorExample Error-401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 404 Not Found
 *
 * @apiErrorExample Error-404:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 500 Internal Server Error
 *
 * @apiErrorExample Error-500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "msg": "Error message"
 *     }
 */
exports.deleteUser = function(req, res, next) {
  let idUpdate = req.params.id;
  let queryString = 'q={"id": '+ idUpdate + '}&';
  let emptyObject = {};

  let clienteMlab = requestJson.createClient(config.URL_MLAB);
  clienteMlab.put('user?' + queryString + config.API_KEY, emptyObject,
    function(error, respuestaMLab, body){
      let response = {};

      if(error) {
          response = {
            "msg" : "Error eliminando usuario."
          }
          res.status(500);
      } else {
        if (body.n > 0) {
          response = {
            "msg" : "Usuario eliminado exitosamente"
          }
          res.status(200);
        } else {
          response = {
            "msg" : "No se encontó el Usuario a eliminar."
          }
          res.status(404);
        }
        res.send(response);
      }
    });
};
