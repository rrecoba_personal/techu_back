// imports config for this project
const config = require('../config/config.js');

// imports required modules
const requestJson = require('request-json');
const bcrypt = require('bcryptjs');
const inputValidator = require('./validator.js');
const util = require('../helpers/utils.js')
const users = require('./users.js')


/**
 * @api {post} /auth/login User Login
 * @apiName Login
 * @apiGroup Authentication
 * @apiVersion 1.0.0
 *
 * @apiParam {String} email User's email
 * @apiParam {String} password User passwordº
 *
 * @apiSuccess {User}  User User info
 * @apiSuccess {jwt}  String JWT token for future calls
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *     "user": {
 *         "_id": {
 *             "$oid": "5c0bdbc61f6e4f682f464fef"
 *         },
 *         "id": 2,
 *         "firstName": "Rodrigo Viana",
 *         "lastName": "Recoba",
 *         "email": "rodri@gmail.com",
 *         "password": "$2a$10$Sx7GMhoKPQEClI4Zxrz2h.vLAUpfUlVztqhlJm8XJjDLUOA47NDKa",
 *         "accounts": [
 *             {
 *                 "id": 326925,
 *                 "name": "Caja de Ahorro UYU",
 *                 "balance": 50,
 *                 "currency": "UYU"
 *             },
 *             {
 *               "id": 2192907,
 *               "name": "Caja de Ahorro USD",
 *               "balance": 697,
 *               "currency": "USD"
 *           }
 *        ]
 *      },
 *      "jwt": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiZW1haWwiOiJyb2RyaUBnbWFpbC5jb20iLCJpYXQiOjE1NDQzODY1MTEsImV4cCI6MTU0NDM5MDExMX0.g8_7ohS-_7LlV5rUr7ZCB2YPdTyUPEPP2s7FgAPXawQ"
 *    }
 *
 * @apiError 401 Unauthorized
 *
 * @apiErrorExample Error-401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 400 Bad Request
 *
 * @apiErrorExample Error-404:
 *     HTTP/1.1 400 Bad Reuqest
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 500 Internal Server Error
 *
 * @apiErrorExample Error-500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "msg": "Error message"
 *     }
 */
exports.login = function(req, res, next) {

  let loginData = {
    "email": req.body.email,
    "password": req.body.password
  }

  // validate api-login inputs
  let result = inputValidator.validateLoginInputs(loginData);

  if (result.valid) {

    // get password from mlab
    users.getUserByMail(loginData.email, function (users, error ) {
      if (!error) {
        let user = users[0];
        if(user != undefined){
          var loginOK = bcrypt.compareSync(loginData.password, user.password);
          if (loginOK) {
            let jwt = util.createJWT(user);
            res.status(200).send({user, jwt});
          } else {
            res.status(401).send({"msg":"Usuario y/o Contraseña incorrectos"});
          }
        } else {
          res.status(401).send({"msg":"Usuario y/o Contraseña incorrectos"});
        }
      } else {
        res.status(500).send({"msg": "Error consultando mlab"})
      }
    });
  } else {
      res.status(400);
      res.send(result.errors);
  }
}




/**
 * @api {post} /auth/logout User Logout
 * @apiName Logout
 * @apiGroup Authentication
 * @apiVersion 1.0.0
 *
 * @apiHeader {String} authorization Users JWT token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhMzE0YmFjNzY1Nzc2MGM1YWRkNTM2MiIsImZpcnN0TmFtZSI6IkFyZWwiLCJsYXN0TmFtZSI6IlNpcmluIiwiZW1haWwiOiJhcmVsc2lyaW4rMUBnbWFpbC5jb20iLCJpYXQiOjE1MTY3MzQyNDgsImV4cCI6MTUxNjc0NDMyOH0.mQzSgw_A5PdFfyRs88BZgyBUvmZhzrrmfw2SZWp6kUw"
 *     }
 * @apiParam {String} email User's email to logut
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *
 *    }
 *
 *
 * @apiErrorExample Error-500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "msg": "Error message"
 *     }
 */
exports.logout = function(req, res, next) {
  var email = req.body.email;
  var queryStringEmail = 'q={"email":"' + email + '"}&' + config.API_KEY;

  clienteMlab = requestJson.createClient(config.URL_MLAB);
  clienteMlab.get('user?' + queryStringEmail,function(error, respuestaMLab, body) {

    if (body.length > 0) {
      var respuesta = body[0];
      if(respuesta != undefined){
            console.log("logout Correcto");

            var session = {"logged":""};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);

            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + config.API_KEY, JSON.parse(logout),
              function(errorP, respuestaMLabP, bodyP) {
                if(!errorP){
                  res.status(200).send();
                }else{
                  res.status(500).send({'msg': 'Ocurrió un error en el login'} );
                };
              });
      } else {
        res.status(500).send({'msg': 'Ocurrió un error en el login'} );
      }
    } else {
      // No doy mayor inforamcion si no se encuentra usuario
      res.status(200).send();
    }
  });
}
