// imports config for this project
const config = require('../config/config.js');
const Transaction = require('../model/transaction.js');
const usersDA = require('../data/users.js')
const inputValidator = require('./validator.js');

// imports required modules
const requestJson = require('request-json');


/**
 * @api {get} /users/:id/accounts/:id/transations Get Transactions

 * @apiName GetTransactions
 * @apiGroup Transaction
 * @apiVersion 1.0.0
 * @apiHeader {String} authorization Users JWT token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhMzE0YmFjNzY1Nzc2MGM1YWRkNTM2MiIsImZpcnN0TmFtZSI6IkFyZWwiLCJsYXN0TmFtZSI6IlNpcmluIiwiZW1haWwiOiJhcmVsc2lyaW4rMUBnbWFpbC5jb20iLCJpYXQiOjE1MTY3MzQyNDgsImV4cCI6MTUxNjc0NDMyOH0.mQzSgw_A5PdFfyRs88BZgyBUvmZhzrrmfw2SZWp6kUw"
 *     }
 *
 *
 * @apiSuccess {Array}  transactions The list of Transactions
 * @apiSuccessExample Success-Response:
 *  [{
 *        "_id": {
 *            "$oid": "5c0d634f1f6e4f682f4ab1bd"
 *        },
 *        "user_id": 2,
 *        "account_id": 326925,
 *        "description": "test",
 *        "amount": 10,
 *        "type": "Credito",
 *        "date": "09/12/2018"
 *    },
 *    {
 *        "_id": {
 *            "$oid": "5c0d635a1f6e4f682f4ab1bf"
 *        },
 *        "user_id": 2,
 *        "account_id": 326925,
 *        "description": "test",
 *        "amount": 10,
 *        "type": "Debito",
 *        "date": "09/12/2018"
 *    }
 *  }]
 *
 * @apiError 500 Internal Server Error
 *
 * @apiErrorExample Error-500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "msg": "Error message"
 *     }
 */
exports.getTransactions = function(req, res, next) {
  let filterField = "f={'_id':0}";
  let queryObject = { "user_id": parseInt(req.params.idUser), "account_id": parseInt(req.params.idAccount)};
  let queryString = 'q=' + JSON.stringify(queryObject) + '&'+ /*filterField + '&'+ */config.API_KEY;

  let clienteMlab = requestJson.createClient(config.URL_MLAB);
  console.log(queryString);
  clienteMlab.get("transaction?" + queryString , function(err, resM, body) {
    if(!err) {
        res.status(200).send(body);
        console.log(body);
    } else {
      res.status(500).send({msg: "Ha ocurrido un error al consultar las tranacciones"});
    }

  });
}

/**
 * @api {post} /users/:id/accounts/:id/transations Add Transaction
 * @apiName AddTransaction
 * @apiGroup Transaction
 * @apiVersion 1.0.0
 *
 * @apiHeader {String} authorization Users JWT token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhMzE0YmFjNzY1Nzc2MGM1YWRkNTM2MiIsImZpcnN0TmFtZSI6IkFyZWwiLCJsYXN0TmFtZSI6IlNpcmluIiwiZW1haWwiOiJhcmVsc2lyaW4rMUBnbWFpbC5jb20iLCJpYXQiOjE1MTY3MzQyNDgsImV4cCI6MTUxNjc0NDMyOH0.mQzSgw_A5PdFfyRs88BZgyBUvmZhzrrmfw2SZWp6kUw"
 *     }
 *
 * @apiParam {Double} amount Transaction amount
 * @apiParam {String} description Transaction description
 * @apiParam {String} type Transaction type (Credito, Debito)
 *
 * @apiSuccess {String}  msg Message
 * @apiSuccess {Transaction}  response Transaction detail
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *     "msg": "Transaccion registrada exitosamente",
 *     "response": {
 *         "user_id": 2,
 *         "account_id": 326925,
 *         "description": "Test API 1",
 *         "amount": 300.12,
 *         "type": "Credito",
 *         "date": "09/12/2018",
 *         "_id": {
 *             "$oid": "5c0d9ff01f6e4f682f4b2d8b"
 *         }
 *     }
 * }
 *
 *
 * @apiError 400 Bad Request
 *
 * @apiErrorExample Error-404:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 401 Unauthorized
 *
 * @apiErrorExample Error-401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "msg": "Error message"
 *     }
 * @apiError 500 Internal Server Error
 *
 * @apiErrorExample Error-500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "msg": "Error message"
 *     }
 */
exports.addTransaction = function(req, res, next) {

  let idUser = parseInt(req.params.idUser);
  let idAccount = parseInt(req.params.idAccount);
  let amount = + parseFloat(req.body.amount).toFixed(2);
  let description = req.body.description;
  let type = req.body.type;
  let newTransaction = new Transaction(idUser, idAccount , description, amount, type);

  let result = inputValidator.validateTranInputs(newTransaction);
  if (result.valid) {

    let clienteMlab = requestJson.createClient(config.URL_MLAB);
    clienteMlab.post("transaction?" + config.API_KEY, newTransaction,
      function(error, respuestaMLab, body){

        let response = {};
        if (error) {
          response = {
            "msg" : "Error al registrar la transaccion."
          }
          res.status(500).send(response);
        } else {
          response = body;
        }
        res.status(200).send({msg: "Transaccion registrada exitosamente", response} );
    });

    // update balance
    usersDA.getUser(idUser, function(user) {

      // updates amount sign
      if (type == 'Debito') {
        amount = amount * -1;
      }

      // updates balance
      let accounts = user.accounts;
      for (var i=0; i<accounts.length; i++){
        if (accounts[i].id == idAccount) {
          accounts[i].balance = accounts[i].balance + amount;
        }
      }

      let queryString = 'q={"id": '+ idUser + '}&' + config.API_KEY;
      clienteMlab.put('user?' + queryString, user,
          function(error, respuestaMLab, body){
            console.log("balance mlab: " + JSON.stringify(body));
          });

    });
  } else {
      res.status(400).send(result.errors);
  }



}
