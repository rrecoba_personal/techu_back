const mailValidator = require('email-validator');

exports.validateLoginInputs = function(loginData){
  var result = {};
  var errorList = [];

  // verifico que el email no sea vacio
  if (!loginData.email){
    errorList.push({"error":"El email es requerido."});
  }

  // verifico que el password no sea vacio
  if (!loginData.password){
    errorList.push({"error":"El password es requerido."});
  }

  result.valid = (errorList.length == 0);
  result.errors = errorList;
  return result;
}


/** Validación de datos */
exports.validateUserInputs = function(userData){
  var result = {};
  var errorList = [];

  //Verifico que el nombre no sea vacio
  if(userData.firstName == undefined){
    errorList.push({"error":"El nombre del usuario es requerido."});
  }

  //Verifico que el apellido no sea vacio
  if(userData.lastName == undefined){
     errorList.push({"error":"El apellido del usuario es requerido."});
  }

  //Verifico el password
  if (userData.password == undefined){
     errorList.push({"error":"El password del usuario es requerido."});
 } else if(userData.password.length < 6){
     errorList.push({"error":"El largo mínimo del password es 6."});
  }

  //Valido email
  if (userData.email == undefined){
    errorList.push({"error":"El mail del usuario es requerido"});
  } else if(!mailValidator.validate(userData.email)){
    console.log(userData.email);
    errorList.push({"error":"El formato del mail no es correcto."});
  }

  result.valid = (errorList.length == 0);
  result.errors = errorList;
  return result;
}

/** Validación de datos */
exports.validateUpdateInputs = function(firstName, lastName, password){
  var result = {};
  var errorList = [];

  //Verifico que el nombre no sea vacio
  if(firstName == undefined){
    errorList.push({"error":"El nombre del usuario es requerido."});
  }

  //Verifico que el apellido no sea vacio
  if(lastName == undefined){
     errorList.push({"error":"El apellido del usuario es requerido."});
  }

  //Verifico el password
  if (password == undefined){
     errorList.push({"error":"El password del usuario es requerido."});
  } else if(password.length < 6){
     errorList.push({"error":"El largo mínimo del password es 6."});
  }

  result.valid = (errorList.length == 0);
  result.errors = errorList;
  return result;
}


/** Validación de datos */
exports.validateTranInputs = function(trn){
  let result = {};
  let errorList = [];

  //Verifico que el importe no sea vacio
  if(trn.amount == undefined ){
    errorList.push({"error":"El importe es requerido."});
  } else {
    let importe = Number(trn.amount);
    if (isNaN(importe)) {
      errorList.push({"error":"El importe es incorrecto."});
    } else if (importe <= 0){
        errorList.push({"error":"El importe debe ser positivo."});
    }
  }

  //Verifico que la descripcion no sea vacia
  if(trn.description == undefined){
     errorList.push({"error":"La descripcion es requerido."});
  }

  //Verifico el account id
  if (trn.account_id == undefined){
     errorList.push({"error":"La cuenta es requerida."});
 }

  //Valido user id
  if (trn.user_id == undefined){
    errorList.push({"error":"El usuario  es requerido"});
  }

  // verifico el tipo de transaccion
  if (trn.type != 'Credito' && trn.type != 'Debito') {
    errorList.push({"error":"Tipo de transacción no soportada"});
  }


  result.valid = (errorList.length == 0);
  result.errors = errorList;
  return result;
}
