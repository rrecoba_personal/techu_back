// imports required modules and initialize express
const express = require("express");
const config  = require("./config/config.js") ;
const authenticator = require('./middleware/check-authentication.js')

// controllers
const AuthenticationController = require('./controllers/authentication');
const UserController = require('./controllers/users');
const TransactionController = require('./controllers/transactions');

module.exports = function(app) {
	// Initializing route groups
	const apiRoutes  = express.Router(),
	      authRoutes = express.Router(),
        userRoutes = express.Router();

	apiRoutes.use('/auth', authRoutes);
    authRoutes.post('/login', AuthenticationController.login);
		authRoutes.post('/logout', authenticator.requireLogin, AuthenticationController.logout);

	apiRoutes.use('/users', userRoutes);
		userRoutes.post('/', UserController.register);
		userRoutes.get('/', authenticator.requireLogin, UserController.getUsers);
		userRoutes.get('/:id', authenticator.requireLogin, UserController.getUserById);
		userRoutes.put('/:id', authenticator.requireLogin,  UserController.updateUser);
		userRoutes.delete('/:id', authenticator.requireLogin, UserController.deleteUser);

		// transactions
		userRoutes.get('/:idUser/accounts/:idAccount/transactions',authenticator.requireLogin, TransactionController.getTransactions);
		userRoutes.post('/:idUser/accounts/:idAccount/transactions', authenticator.requireLogin,TransactionController.addTransaction);
		
  // Set url for API group routes
	app.use(config.URI , apiRoutes);
};
