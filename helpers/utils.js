const jwt = require('jsonwebtoken');
const config = require('../config/config.js')


exports.createJWT = function createJWT(user) {
  const token = jwt.sign(
    {
      id: user.id,
      email: user.email
    },
    config.JWT_SECRET,
    {
      expiresIn: config.JWT_EXPIRE
    }
  );

  return token;
}

exports.getDate = function getDate() {
  let date = new Date();

/*
  let hour = date.getHours();
   hour = (hour < 10 ? "0" : "") + hour;

   let min  = date.getMinutes();
   min = (min < 10 ? "0" : "") + min;

   let sec  = date.getSeconds();
   sec = (sec < 10 ? "0" : "") + sec;
*/
   let year = date.getFullYear();

   let month = date.getMonth() + 1;
   month = (month < 10 ? "0" : "") + month;

   let day  = date.getDate();
   day = (day < 10 ? "0" : "") + day;

   return  day + "/" + month + "/" + year; // ":" + hour + ":" + min + ":" + sec;
}
