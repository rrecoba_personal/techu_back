// imports config for this project
const config = require('../config/config.js');

// imports required modules
const requestJson = require('request-json');

exports.getUser = function getUser(idUser, callback) {
  var user = {};
  let query = 'q={"id":' + idUser + '}&';
  let queryFilter = query + 'f={"_id":0}&' + config.API_KEY;
  let httpClient = requestJson.createClient(config.URL_MLAB);

  httpClient.get("user?" + queryFilter , function(err, resM, body) {

    if(!err) {
      if (body.length > 0) {
        user = body[0];
      }
    }

    callback(user);
  });
}
